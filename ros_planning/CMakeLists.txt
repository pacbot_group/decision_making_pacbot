cmake_minimum_required(VERSION 3.9.1)
########################################################################################################################
# Metadata
########################################################################################################################
# Read the package manifest.
file(READ "${CMAKE_CURRENT_SOURCE_DIR}/package.xml" package_xml_str)

# Extract project name.
if(NOT package_xml_str MATCHES "<name>([A-Za-z0-9_]+)</name>")
  message(FATAL_ERROR "Could not parse project name from package manifest (aborting)")
else()
  set(extracted_name ${CMAKE_MATCH_1})
endif()

# Extract project version.
if(NOT package_xml_str MATCHES "<version>([0-9]+.[0-9]+.[0-9]+)</version>")
  message(FATAL_ERROR "Could not parse project version from package manifest (aborting)")
else()
  set(extracted_version ${CMAKE_MATCH_1})
endif()

########################################################################################################################
# CMake project
########################################################################################################################
project(${extracted_name} VERSION ${extracted_version} LANGUAGES CXX)

#===========================================================
# Dependencies
#===========================================================
#-----------------------------
# Catkin packages
#-----------------------------
# Load catkin and all dependencies required for this package
find_package(catkin REQUIRED COMPONENTS
  rospy
  message_generation
  std_msgs
)

catkin_python_setup()

#===========================================================
# Generate services in the 'srv' folder
#===========================================================
add_service_files(
  FILES
  Act.srv
  Plan.srv
  UpdatePlan.srv
)
#===========================================================
# Generate added messages and services with 
# any dependencies listed here
#===========================================================
# Generate added messages and services with any dependencies listed here
generate_messages(
  DEPENDENCIES
  std_msgs
)

# Declare catkin package
#===========================================================
# Catkin package specific configurations
#===========================================================
catkin_package(
  CATKIN_DEPENDS
  rospy
  std_msgs
  message_runtime
  # LIBRARIES ${PROJECT_NAME}
)

catkin_install_python(PROGRAMS
  src/${PROJECT_NAME}/problem_generator.py
  src/${PROJECT_NAME}/ros_planning_node.py
  src/${PROJECT_NAME}/pddl_problem_iface.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
#===========================================================
# Install
#===========================================================
install(
  DIRECTORY model downward launch database
  DESTINATION "${CATKIN_PACKAGE_SHARE_DESTINATION}"
)
#===========================================================
