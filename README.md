# decision_making_pacbot

Decision Making Repo

## Getting started

1. Run Ros Master
```sh
$ roscore
```

2. Launch Planning Server
```sh
$ . devel/setup.bash
$ roslaunch ros_planning ros_planning.launch
```
